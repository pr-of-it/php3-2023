COMPOSE=docker-compose -f docker-compose.yaml
DEPLOY=docker stack deploy --compose-file docker-compose.yaml php3-test --prune --with-registry-auth

build:
	$(COMPOSE) build

push: build
	$(COMPOSE) push

up:
	$(COMPOSE) up -d

deploy:
	DOCKER_HOST=212.193.63.48 $(DEPLOY)